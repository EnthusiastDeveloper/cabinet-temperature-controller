#!/usr/bin/env python3
"""
This file is the main script that executes the the temperature controlling system.
The system is sampling the ambient temperature and controlling fans via relays if temperatures exceeds certain threshold


Algorithm is as follows:

Trigger via event
Make sure DB exists and accessible
For each sensor in DB:
	Read sensor's temperature
	Add row to DB with reported temperature
	Get temp thresholds from configuration file


	Option #1:
	If temperature exceeds upper threshold:
		If the corresponding fan is idle:
			Activate the corresponding fan
			Add row to DB regarding fan state change
	Else if temperature drops below lower threshold:
		If the corresponding fan is working:
			Deactivate the corresponding fan
			Add row to DB regarding fan state change


	Option #2:
	Based on all sensors' readings and thresholds decide which fan should be activated and for how long

"""

import json
import logging
import os
import RPi.GPIO as GPIO
from time import sleep
from sqlite_wrapper import SqliteWrapper
from sensor.sensor import Sensor
from Fan.fan import Fan

configurations_file_name = 'config.json'
configurations = {}
logger = logging
is_db_exist = False
db = SqliteWrapper()
sensors_os_path = '/sys/bus/w1/devices'
connected_sensors = []
fans = {}


def initialize_logger():
	global logger
	try:
		log_name = configurations['setup']['logging']['logger_name']
		log_level = configurations['setup']['logging']['log_level']
		if log_level.lower() == 'off':
			logger = None
			return

		# Create application logger
		logger = logging.getLogger(log_name)
		logger.setLevel(logging.DEBUG)
		# Create file handler
		fh = logging.FileHandler(f'{log_name}.log', mode='w')
		fh.setLevel(log_level.upper())
		# Create console handler
		ch = logging.StreamHandler()
		ch.setLevel(log_level.upper())
		# Create formatter and add it to the handlers
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		fh.setFormatter(formatter)
		ch.setFormatter(formatter)
		# Add file handler to the logger
		logger.addHandler(fh)
		logger.addHandler(ch)
	except ValueError:
		print('Fatal error while creating logger file.')
		logger = None


def write_log(level, message: str):
	global logger
	if logger is not None:
		logger.log(level, message)


def connect_to_system():
	global db
	write_log(logging.INFO, 'Initialize the system...')
	db = get_db_connection()
	initialize_sensors()
	initialize_fans()
	db.commit()
	write_log(logging.INFO, 'System initialization completed.')


def get_db_connection() -> SqliteWrapper:
	global is_db_exist
	_db = SqliteWrapper.get_instance()
	db_name = configurations['setup']['db']['name']
	if db_name in os.listdir('.'):
		is_db_exist = True
		write_log(logging.INFO, 'Connecting to existing DB')
		_db.connect(os.path.join(os.getcwd(), db_name))
		write_log(logging.INFO, 'DB connection is ready')
	else:
		is_db_exist = False
		write_log(logging.DEBUG, 'Creating the database file, this might take a few seconds.')
		creation_script = os.path.join(os.getcwd(), 'create_temp_control_db.sql')
		_db.initialize_db(creation_script, db_name)
	return _db


def initialize_sensors():
	global is_db_exist
	global db
	try:
		devices = os.listdir(sensors_os_path)
		for position, sensor_id in configurations['setup']['sensors_id'].items():
			if sensor_id in devices:
				write_log(logging.DEBUG, f'{sensor_id} found, adding to list.')
				# Create new Sensor object and save it in sensors list.
				_sensor = Sensor(sensor_id, configurations['setup']['logging']['logger_name'])
				connected_sensors.append([_sensor, position])
				if is_db_exist and not db.is_sensor_exists(sensor_id, position):
					write_log(logging.DEBUG, 'Sensor {sensor_id} is missing from DB. Adding it now.')
				db.insert_new_sensor(sensor_id, position)
				write_log(logging.DEBUG, f'{sensor_id} process completed.')
			else:
				write_log(logging.ERROR, f'The sensor {sensor_id} is registered but could not be found on the system.')
		write_log(logging.INFO, f'{len(connected_sensors)} sensors successfully registered.')
	except FileNotFoundError:
		write_log(logging.CRITICAL, f'No such file or directory {sensors_os_path}')
		exit(-1)


def initialize_fans():
	GPIO.setmode(GPIO.BOARD)  # This refers pin numbers as their enumeration on the board
	config_fans = configurations['setup']['fans']
	possible_positions = db.read_positions()
	for (fan_position, fan_pin) in config_fans.items():
		for position in possible_positions:
			if position.lower() in fan_position.lower():
				db.insert_new_fan(position)
				fans[position.lower()] = Fan(fan_pin, configurations['setup']['logging']['logger_name'])


def shutdown():
	for (position, fan) in fans.items():
		fan.turn_off()
		db.insert_fan_state(fan_position=position, is_fan_spinning=False)
	GPIO.cleanup()
	db.close()
	exit(0)


try:
	with open(configurations_file_name, mode='r') as conf:
		configurations = json.load(conf)
except FileNotFoundError:
	print('Failed to find the configurations file. Can\'t continue.')
	exit(-1)

initialize_logger()
connect_to_system()
write_log(logging.INFO, f"Start reading temperatures reading every {configurations['temperatures']['seconds_between_polling']} seconds")
while True:
	try:
		for sensor, position in connected_sensors:
			fan = fans[position]
			temp = sensor.read_data()
			db.insert_sensor_reading(sensor_id=sensor.device_id, readout=temp)
			if temp > configurations['temperatures']['single_fan_activation'] and not fan.is_spinning():
				fan.turn_on()
				db.insert_fan_state(fan_position=position, is_fan_spinning=True)
			elif temp < configurations['temperatures']['turn_off_threshold'] and fan.is_spinning():
				fan.turn_off()
				db.insert_fan_state(fan_position=position, is_fan_spinning=False)
		sleep(configurations['temperatures']['seconds_between_polling'])
	except KeyboardInterrupt:
		write_log(logging.INFO, 'Stopping execution due to user request')
		shutdown()
