/*---------------------------------------------------------
-- Temperature controller database initialization script --
---------------------------------------------------------*/

---- Static data regarding positions of devices in the setup

-- Create positions table and fill it with initial values
CREATE TABLE IF NOT EXISTS position (
	id INTEGER PRIMARY KEY,
	position_name TEXT NOT NULL UNIQUE
);

INSERT OR IGNORE INTO position(position_name) VALUES
('Left'),
('Center'),
('Right');

-- Create fan actions table and fill it with initial values
CREATE TABLE IF NOT EXISTS fan_action (
	id INTEGER PRIMARY KEY,
	action TEXT NOT NULL UNIQUE
);

INSERT OR IGNORE INTO fan_action(action) VALUES
('Stop'),
('Start');


---- The following tables will be filled during the system initialization

-- Sensors table, contains the devices connected to the system:
CREATE TABLE IF NOT EXISTS sensor (
	id INTEGER PRIMARY KEY,
	sensor_id TEXT UNIQUE,
	position_id INTEGER NOT NULL,
	FOREIGN KEY(position_id) REFERENCES position(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

-- Fans table, contains the fans connected to the system:
CREATE TABLE IF NOT EXISTS fan (
	id INTEGER PRIMARY KEY,
	position_id INTEGER NOT NULL,
	FOREIGN KEY(position_id) REFERENCES position(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

---- System runtime data

-- Create sensors readings table
CREATE TABLE IF NOT EXISTS readings (
	id INTEGER PRIMARY KEY,
	sensor_id INTEGER NOT NULL,
	timestamp TEXT NOT NULL,
	temperature REAL NOT NULL,
	FOREIGN KEY(sensor_id) REFERENCES sensor(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

-- Create association table, describing which fan changed its state and when
CREATE TABLE IF NOT EXISTS fan_activity (
	id INTEGER PRIMARY KEY,
	fan_id INTEGER NOT NULL,
	action_id INTEGER NOT NULL,
	timestamp TEXT NOT NULL,
	FOREIGN KEY(fan_id) REFERENCES fan(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY(action_id) REFERENCES fan_action(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);
