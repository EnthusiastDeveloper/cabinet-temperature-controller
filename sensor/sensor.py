#!/usr/bin/env python3

import logging
import subprocess

w1_bus_path = '/sys/bus/w1/devices/{0}/w1_slave'


class Sensor:
	def __init__(self, _device_id: str, logger: str = None):
		if _device_id is not None:
			self.device_id = _device_id
			self.logger = logging.getLogger(logger)

	def get_name(self) -> str:
		return self.device_id

	def read_data(self) -> float:
		temp_readout = -1
		try:
			file_path = w1_bus_path.format(self.device_id)
			# Read the sensor's data file. Last line contains its temperature readings
			line = subprocess.check_output(['tail', '-1', file_path])
			line = str(line, encoding='utf-8')
			# cut out the string part that holds the temperature, eliminate trailing newline
			temp = line[line.find('t=') + 2:-1]
			temp_readout = float(temp) / 1000.0
			return temp_readout
		except TypeError:
			self.logger.exception(f'{self.device_id} got Type Error')
		except ValueError:
			self.logger.exception(f'{self.device_id} got Value Error')
		except subprocess.SubprocessError:
			self.logger.exception(f'{self.device_id} encountered subprocess error')
		except Exception:
			self.logger.exception(f'{self.device_id} got some error while trying to read the temperature.')
		finally:
			return temp_readout
