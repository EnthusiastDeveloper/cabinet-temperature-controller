#!/usr/bin/env python3

import logging
import RPi.GPIO as GPIO


class Fan:
	def __init__(self, control_pin: int, logger: str = None):
		self.control_pin = control_pin
		GPIO.setup(control_pin, GPIO.OUT)
		self.logger = logging.getLogger(logger)
		self.is_on = False
		self.turn_off()
		self.logger.info(f'Fan on pin #{control_pin} initialized.')

	def turn_on(self):
		if not self.is_on:
			GPIO.output(self.control_pin, 1)
			self.is_on = True

	def turn_off(self):
		if self.is_on:
			GPIO.output(self.control_pin, 0)
			self.is_on = False

	def is_spinning(self) -> bool:
		return self.is_on
