#!/usr/bin/env python3

import sqlite3


class SqliteWrapper:
	"""
	A Class used to wrap sqlite database operations.
	This class implements a Singleton design pattern, which ensures
	only one connection to the database at a given moment. Hopefully it
	will help avoid read/write races in the future.

	This class has methods specifically accustomed to the project's needs,
	so re-using it in other projects will require some tweaks.
	"""

	# region Singleton implementation
	__instance = None

	@staticmethod
	def get_instance():
		""" Static access method. """
		if SqliteWrapper.__instance is None:
			SqliteWrapper()
		return SqliteWrapper.__instance

	def __init__(self):
		""" Virtually private constructor. """
		if SqliteWrapper.__instance is not None:
			raise Exception("This class is a singleton!")
		else:
			SqliteWrapper.__instance = self
			self.connection = None
			self.cursor = None

	def __del__(self):
		if self.connection is not None:
			self.connection.close()

	# endregion

	# region Initialize, connect and close connection

	def initialize_db(self, script_path: str, db_name: str):
		self.connection = sqlite3.connect(db_name)
		self.cursor = self.connection.cursor()
		with open(script_path, mode='r') as script:
			query = script.read()
			self.cursor.executescript(query)
		self.connection.commit()

	def connect(self, database_file):
		try:
			self.connection = sqlite3.connect(database_file)
			self.cursor = self.connection.cursor()
		except FileNotFoundError as fnf:
			print(f'Could not find file {database_file}')
			raise fnf
		except Exception:
			print('Some exception occurred while getting db cursor')

	def has_db_changed(self) -> bool:
		try:
			return self.connection.in_transaction
		except AttributeError:
			return False

	def close(self, save_changes=True):
		if save_changes and self.has_db_changed():
			self.connection.commit()
		self.cursor.close()
		self.connection.close()
		self.cursor = None
		self.connection = None

	def commit(self):
		self.connection.commit()

	# endregion

	# Add additional functionality here:
	# CRUD = Create, Read, Update, Delete

	def read_positions(self) -> list:
		return [p[0] for p in self.cursor.execute('SELECT position_name from position')]

	def get_position_id(self, position: str) -> int:
		self.cursor.execute('SELECT id FROM position WHERE position_name like ?', ('%' + position + '%',))
		try:
			return self.cursor.fetchone()[0]
		except TypeError:
			raise ValueError(f'Wrong value: "{position}"')

	def get_action_id(self, action: str) -> int:
		self.cursor.execute('SELECT id FROM fan_action WHERE action like ?', ('%' + action + '%' ,))
		try:
			return self.cursor.fetchone()[0]
		except TypeError:
			raise ValueError(f'Wrong value: "{action}"')

	def read_fan_actions(self) -> list:
		return [p[0] for p in self.cursor.execute('SELECT action from fan_action')]

	def get_sensor_id(self, sensor_id: str) -> int:
		self.cursor.execute('SELECT id FROM sensor WHERE sensor_id like ?', ('%' + sensor_id + '%',))
		try:
			return self.cursor.fetchone()[0]
		except TypeError:
			raise ValueError(f'Wrong value: "{sensor_id}"')

	def get_fan_id(self, fan_position: str) -> int:
		self.cursor.execute(
			'SELECT fan.id FROM fan, position WHERE fan.position_id = position.id and position_name like ?',
			('%' + fan_position + '%',))
		try:
			return self.cursor.fetchone()[0]
		except TypeError:
			raise ValueError(f'Wrong value: "{fan_position}"')

	def insert_new_sensor(self, sensor_id: str, position: str):
		if not self.is_sensor_exists(sensor_id, position):
			position_id = self.get_position_id(position)
			self.cursor.execute('INSERT into sensor(sensor_id, position_id) VALUES (?,?)', (sensor_id, position_id))

	def is_sensor_exists(self, sensor_id: str, position: str) -> bool:
		try:
			position_id = self.get_position_id(position)
			self.cursor.execute('SELECT * FROM sensor WHERE sensor_id like ?', ('%' + sensor_id + '%',))
			res = self.cursor.fetchone()
			return res is not None and res[2] == position_id
		except TypeError:
			return False

	def get_sensor_direction(self, sensor_id: str) -> str:
		self.cursor.execute('SELECT position_id FROM sensor WHERE sensor_id like ?', ('%' + sensor_id + '%',))
		try:
			position_id = self.cursor.fetchone()[0]
			self.cursor.execute('SELECT position_name FROM position WHERE id like ?', ('%' + str(position_id) + '%',))
			return self.cursor.fetchone()[0]
		except TypeError:
			raise ValueError(f'Wrong value: "{sensor_id}"')

	def insert_new_fan(self, position: str):
		if not self.is_fan_exists(position):
			position_id = self.get_position_id(position)
			self.cursor.execute('INSERT into fan(position_id) VALUES (?)', (position_id,))

	def is_fan_exists(self, fan_position: str) -> bool:
		try:
			return self.get_fan_id(fan_position) is not None
		except TypeError:
			return False

	def insert_sensor_reading(self, sensor_id: str, readout: float):
		sensor_id = self.get_sensor_id(sensor_id)
		self.cursor.execute(
			'INSERT into readings(sensor_id, timestamp, temperature) VALUES (?, datetime("now","localtime"),?)',
			(sensor_id, readout))

	def insert_fan_state(self, fan_position: str, is_fan_spinning: bool):
		fan_id = self.get_fan_id(fan_position)
		fan_actions = self.read_fan_actions()
		action_id = self.get_action_id(fan_actions[is_fan_spinning])
		self.cursor.execute(
			'INSERT into fan_activity(fan_id, action_id, timestamp) VALUES (?, ?, datetime("now","localtime"))',
			(fan_id, action_id))
